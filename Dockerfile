FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code /run/monica/cache
WORKDIR /app/code

RUN wget https://github.com/monicahq/monica/archive/v2.13.0.tar.gz -O -| tar -xz -C /app/code --strip-components=1 && \
    chown -R www-data:www-data /app/code

# https://github.com/monicahq/monica/wiki/Installing-Monica-(Generic)
RUN apt-get update && apt-get install -y php7.2-bcmath && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN chmod -R g+w bootstrap/cache
RUN sudo -u www-data composer install --no-interaction --prefer-dist --no-suggest --optimize-autoloader --no-dev && \
    sudo -u www-data composer clear-cache

RUN npm install && npm run production && chown -R www-data:www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/monica.conf /etc/apache2/sites-enabled/monica.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite

# Note that the sessions stuff is unused because monica uses lavarel sessions (storage/framework/sessions)
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 25M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 25M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 64M

RUN chmod -R g+rw /app/code/storage \
    && mv /app/code/storage /app/code/storage.template && ln -s /app/data/storage /app/code/storage \
    && mv /app/code/bootstrap/cache /app/code/bootstrap/cache.template && ln -s /run/monica/bootstrap-cache /app/code/bootstrap/cache \
    && ln -s /app/data/env /app/code/.env \
    && ln -s /app/code/storage/app/public /app/code/public/storage

ADD env.production start.sh /app/code/

CMD [ "/app/code/start.sh" ]
