#!/bin/bash

set -eu

mkdir -p /run/monica/sessions /run/monica/bootstrap-cache /run/monica/framework-cache /run/monica/logs

readonly ARTISAN="sudo -E -u www-data php /app/code/artisan"

if [[ ! -f /app/data/.cr ]]; then
    echo "=> First run"
    #Taken from installation docs: https://github.com/monicahq/monica/wiki/Installing-Monica-(Generic)
    mkdir -p /app/data/storage
    cp -R /app/code/storage.template/* /app/data/storage
    cp /app/code/env.production /app/data/env

    chown -R www-data:www-data /run/monica /app/data

    echo "=> Generating app key"
    $ARTISAN key:generate --force --no-interaction

    echo "=> Run migrations and seed database"
    $ARTISAN setup:production --force
    echo "=> Create the access tokens required for the API"
    $ARTISAN passport:install
    touch /app/data/.cr
else
    echo "=> Existing installation. Running migration script"
    chown -R www-data:www-data /run/monica /app/data
    $ARTISAN migrate --force
fi

# sessions, logs and cache
[[ -d /app/data/storage/framework/sessions ]] && rm -rf /app/data/storage/framework/sessions
ln -sf /run/monica/sessions /app/data/storage/framework/sessions
rm -rf /app/data/storage/framework/cache && ln -s /run/monica/framework-cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/monica/logs /app/data/storage/logs

# clear cached stuff under /app/data/storage/framework
echo "=> Clearing cache"
$ARTISAN view:clear
$ARTISAN cache:clear

echo "=> Starting Apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

