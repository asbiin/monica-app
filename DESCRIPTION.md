This app packages MonicaHQ <upstream>2.13.0</upstream>.

Monica is an open-source web application to organize the
interactions with your loved ones. I call it a PRM, or
Personal Relationship Management. Think of it as a CRM
(a popular tool used by sales teams in the corporate world)
for your friends or family.

## Purpose

Monica allows people to keep track of everything that's important about their friends and family. Like the activities done with them. When you last called someone. What you talked about. It will help you remember the name and the age of the kids. It can also remind you to call someone you haven't talked to in a while.

## Features

* Add and manage contacts
* Add significant others and children
* Auto reminders for birthdays
* Reminders are sent by email
* Management of debts
* Ability to add notes to a contact
* Ability to indicate how you've met someone
* Management of activities done with a contact
* Management of tasks
* Management of gifts
* Management of addresses and all the different ways to contact someone
* Management of contact field types
* Management of contact pets
* Basic journal
* Ability to indicate how the day went
* Export and import of data
* Export a contact as vCard
* Ability to set custom genders
* Multi users
* Labels to organize contacts
* Multi currencies
* Multi languages
* An API that covers most of the data

