# Monica Cloudron App

This repository contains the Cloudron app package source for [Monica](https://monicahq.com).

Monica is a great open source personal relationship management system.

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=com.monicahq.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.monicahq.cloudronapp@latest
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd monica-app

cloudron build
cloudron install
```

## Logging

Log::debug('An informational message.');

It will appear in `/app/data/storage/logs`.

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, backup and restore. 

```
cd monica-app/test

npm install
mocha --bail test.js
```

